from setuptools import setup, find_packages

setup(
    name="URLOpen",
    version="0.1",
    description="simple application to open urls in specified browsers",
    url="https://gitlab.com/ComCurio/urlopen",
    author="Com Curi",
    author_email="comradecurio@vivaldi.net",
    license='GPL3',
    packages=find_packages(),
    install_requires=["argparse"],
    entry_points={
        'console_scripts': [
            'urlopen=urlopen.urlopen:main'
        ]},
    package_data={
        'urlopen':['*.conf']
        }
)
