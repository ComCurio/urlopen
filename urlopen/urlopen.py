import os
import argparse
import sys


def defBrowser(value):  # finds the browsers for the browsers dictionary
    text = value.split(" = ")
    return(text[0].strip("BROWSER "))


def defUrls(value):    # find the urls for the urls dictionary
    text = value.split(" = ")
    return(text[1])


def openBrowser(url):  # Opens URL with the specified browser, if browser isn't in the list opens with default browser
    info = configRead()
    defaultBrowser = info[0]
    browserDict = info[1]
    urlsDict = info[2]
    exited = 0
    for i in range(len(urlsDict)):
        urlList = urlsDict["UrlsForBrowser"+str(i)].split(',')
        for item in urlList:
            if item.strip() in url and item != "":
                os.system(browserDict["Browser"+str(i)] + " " + url)
                exited = 1
                break
    if exited == 0:
        os.system(defaultBrowser + ' ' + url)
    sys.exit()

# Creating default config file


def checkForConf():
    confPath = (os.getenv("HOME")+"/.config/urlopen.conf")
    if os.path.isfile(confPath) != True:
        confFile = open(confPath, 'w')
        defaultConf = open(os.path.join(os.path.abspath(
            os.path.dirname(__file__)), 'defaultConfig.conf'), 'r')
        confFile.write(defaultConf.read())
        confFile.close()
        defaultConf.close()
        print("default config file written to " + conf)
        sys.exit()

# Opening and formatting the config file


def configRead():

    confPath = (os.getenv("HOME")+"/.config/urlopen.conf")
    configFile = open(confPath, 'r')
    config = configFile.read()
    config = config.strip()
    config = config.split('\n')

    browserDict = {}
    urlsDict = {}

    for i, value in enumerate(config):
        if value == '\n':  # if it is just a blank newline file it is deleted
            del config[i]

        elif value.startswith("DEFAULT_BROWSER"):
            defaultBrowser = value.strip("DEFAULT_BROWSER =")

        elif value.startswith("BROWSER"):
            browserDict["Browser" + str(len(browserDict))] = defBrowser(value)
            urlsDict["UrlsForBrowser" + str(len(urlsDict))] = defUrls(value)

    return([defaultBrowser, browserDict, urlsDict, ])
    configFile.close  # closes config file


def main():
    checkForConf()
    # argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'url', type=str, help='URL that is opened by the configured browser or default browser if url is unassigned')
    args = parser.parse_args()

    openBrowser(args.url)  # opens URL from parsed args


if __name__ == "__main__":
    main()
